<!DOCTYPE html>
<html>
<head>
	<title>Ventas</title>
<script type="text/javascript">
$(document).ready(function(){
	// Write on keyup event of keyword input element
	$("#search7").keyup(function(){
		// When value of the input is not blank
		if( $(this).val() != "")
		{
			// Show only matching TR, hide rest of them
			$("#table7 tbody>tr").hide();
			$("#table7 td:contains-ci('" + $(this).val() + "')").parent("tr").show();
		}
		else
		{
			// When there is no input or clean again, show everything back
			$("#table7 tbody>tr").show();
		}
	});
});
// jQuery expression for case-insensitive filter
$.extend($.expr[":"], 
{
    "contains-ci": function(elem, i, match, array) 
	{
		return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
	}
});
</script>
</head>
<body>
<div class="input-group stylish-input-group">
	<span class="input-group-addon">
    	<span class="glyphicon glyphicon-search"></span>
    </span>
	
	<input class="flexsearch--input" type="text" placeholder="Busqueda" id="search7">
</div>
<?php  
//tomamos los datos del archivo conexion.php  
include("conexion.php");  
//se envia la consulta  
$result = mysqli_query($conect, "SELECT usuarios.Nombre, usuarios.Paterno, COUNT(angelopolis.AuthCode), SEC_TO_TIME( SUM( TIME_TO_SEC( angelopolis.Call_duration ) ) ) FROM usuarios, angelopolis WHERE Call_start BETWEEN '$desde' AND '$hasta' AND usuarios.deptoTari = 'VENTAS' AND usuarios.Empresa LIKE'%GARCIA PINEDA ANGELOPOLIS%' AND angelopolis.AuthCode = usuarios.AuthCode GROUP BY usuarios.Nombre");  
//se despliega el resultado
echo $desde. " - " .$hasta;
echo "<div class='table table-bordered'>";
echo "<table id='table7' class='table table-hover'>";
echo "<thead><tr>";  
echo "<th>Nombre</th>";  
echo "<th>Paterno</th>";  
echo "<th>Eventos</th>";
echo "<th>Duracion</th>";   
echo "</tr></thead>";  

	while ($row = mysqli_fetch_row($result)){
	    echo "<tr>";  
	    echo "<td>$row[0]</td>";  
	    echo "<td>$row[1]</td>";  
	    echo "<td>$row[2]</td>";
	    echo "<td>$row[3]</td>"; 
	    echo "</tr>";
	}

echo "</table>";
echo "</div>";
?>
</body>
</html>