<?php
session_start();
include("conexion.php");

if(isset($_SESSION['nom_usr'])) {
    # code...
  } else {
  echo '<script> window.location="login"; </script>';
}

$desde = ($_POST['desde']);
$hasta = ($_POST['hasta']);

if ($desde == NULL || $hasta == NULL) {
  echo "<script> window.location='index'; </script>";
}

?>

  <div style="padding-left: 50px; padding-top: 10px; float: left">
      <b>Desde: </b> <?php echo $desde; ?> - <b>Hasta: </b> <?php echo $hasta; ?>
  </div>
  <div style="text-align: right; padding-top: 10px; padding-right: 50px">
  <b>Bienvenido: </b> <?php echo $_SESSION['nombre']; ?> | 
  <a href="logout"><u>Salir </u></a><span class="glyphicon glyphicon-log-out"></span>
  </div>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Reporte de llamadas</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: 'Eventos'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Eventos',
            data: [
      <?php     
      $sql= mysqli_query($conect, "SELECT usuarios.deptoTari, COUNT(angelopolis.AuthCode), SEC_TO_TIME( SUM( TIME_TO_SEC( angelopolis.Call_duration ) ) ) FROM usuarios, angelopolis WHERE Call_start BETWEEN '$desde' AND '$hasta' AND usuarios.deptoTari <> '' AND usuarios.Empresa LIKE'%GARCIA PINEDA ANGELOPOLIS%' AND angelopolis.AuthCode = usuarios.AuthCode GROUP BY usuarios.deptoTari");
      while($res=mysqli_fetch_array($sql)){        
      ?>
      
                ['<?php echo $res['0'];?>', <?php echo $res['1'] ?> ],
      <?php
      }
      ?>
              
            ]
        }]
    });
});


$(function () {
    $('#gra_duration').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: 'Duracion'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Duracion',
            data: [
      <?php     
      $sql1= mysqli_query($conect, "SELECT usuarios.deptoTari, COUNT(angelopolis.AuthCode), SEC_TO_TIME( SUM( TIME_TO_SEC( angelopolis.Call_duration ) ) ) FROM usuarios, angelopolis WHERE Call_start BETWEEN '$desde' AND '$hasta' AND usuarios.deptoTari <> '' AND usuarios.Empresa LIKE'%GARCIA PINEDA ANGELOPOLIS%' AND angelopolis.AuthCode = usuarios.AuthCode GROUP BY usuarios.deptoTari");
      while($res1=mysqli_fetch_array($sql1)){        
      ?>
        <?php
            $hora = $res1['2'];
            list($horas, $minutos, $segundos) = explode(':', $hora);
             ?>
                ['<?php echo $res1['0'];?>', <?php echo $horas ?> ],
      <?php
      }
      ?>
              
            ]
        }]
    });
});

</script>

<script type="text/javascript">
$(document).ready(function(){
  // Write on keyup event of keyword input element
  $("#kwd_search").keyup(function(){
    // When value of the input is not blank
    if( $(this).val() != "")
    {
      // Show only matching TR, hide rest of them
      $("#my-table tbody>tr").hide();
      $("#my-table td:contains-ci('" + $(this).val() + "')").parent("tr").show();
    }
    else
    {
      // When there is no input or clean again, show everything back
      $("#my-table tbody>tr").show();
    }
  });
});
// jQuery expression for case-insensitive filter
$.extend($.expr[":"], 
{
    "contains-ci": function(elem, i, match, array) 
  {
    return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
  }
});
</script>
</head>
<body>

<div class="container">
<a href="index" title="Inicio" style="text-decoration:none;">
  <h2>Garcia Pineda Angelopolis S.A. de C.V.</h2></a>
  <ul class="nav nav-pills">
    <li class="active"><a data-toggle="pill" href="#home">Resumen</a></li>
    <li><a data-toggle="pill" href="#menu1">Administracion</a></li>
    <li><a data-toggle="pill" href="#menu2">BDC Postventa</a></li>
    <li><a data-toggle="pill" href="#menu3">Citas</a></li>
    <li><a data-toggle="pill" href="#menu4">Mercadotecnia</a></li>
    <li><a data-toggle="pill" href="#menu5">Refacciones</a></li>
    <li><a data-toggle="pill" href="#menu6">Seguros</a></li>
    <li><a data-toggle="pill" href="#menu7">Servicio</a></li>
    <li><a data-toggle="pill" href="#menu8">Ventas</a></li>
    <li><a data-toggle="pill" href="#menu9">BDC Ventas</a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <h3>Resumen</h3>

<!-- **************************** Grafica Eventos **************************************** -->

<script src="js/highcharts.js"></script>
<script src="js/modules/exporting.js"></script>

<div id="container" style="min-width: 410px; height: 400px; max-width: 600px; margin: 0 auto; float: left;"></div><br>


<!-- **************************** Grafica Duracion **************************************** -->

<div id="gra_duration" style="min-width: 410px; height: 400px; max-width: 600px; margin: 0 auto; float: right;"></div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

<!-- ****************************Reporte General**************************************** -->
      
        <?php include 'General.php'; ?>

    </div>

<!-- **************************** Administracion **************************************** -->

    <div id="menu1" class="tab-pane fade">
      <h3>Administracion</h3>
      
        <?php include 'Administracion.php'; ?>

    </div>

<!-- **************************** BDC Postventa **************************************** -->

    <div id="menu2" class="tab-pane fade">
      <h3>BDC Postventa</h3>
        
        <?php include 'bdc_postventa.php'; ?>

    </div>

<!-- **************************** Citas **************************************** -->

    <div id="menu3" class="tab-pane fade">
      <h3>Citas</h3>
      
        <?php include 'Citas.php'; ?>

    </div>

<!-- **************************** Marketing **************************************** -->

    <div id="menu4" class="tab-pane fade">
      <h3>Mercadotecnia</h3>
      
        <?php include 'Mercadotecnia.php'; ?>

    </div>

<!-- **************************** Refacciones **************************************** -->

    <div id="menu5" class="tab-pane fade">
      <h3>Refacciones</h3>
      
        <?php include 'Refacciones.php'; ?>

    </div>

<!-- **************************** Seguros **************************************** -->

    <div id="menu6" class="tab-pane fade">
      <h3>Seguros</h3>
      
        <?php include 'Seguros.php'; ?>

    </div>

<!-- **************************** Servicio **************************************** -->

    <div id="menu7" class="tab-pane fade">
      <h3>Servicio</h3>
      
        <?php include 'Servicio.php'; ?>

    </div>    

<!-- **************************** Ventas **************************************** -->

    <div id="menu8" class="tab-pane fade">
      <h3>Ventas</h3>
      
        <?php include 'Ventas.php'; ?>

    </div>

<!-- **************************** BDC Ventas **************************************** -->

    <div id="menu9" class="tab-pane fade">
      <h3>BDC Ventas</h3>

        <?php include 'bdc_ventas.php'; ?>

    </div>

  </div>
</div>
<!--
http://www.w3schools.com/bootstrap/bootstrap_tabs_pills.asp
http://www.w3schools.com/bootstrap/bootstrap_tables.asp
-->
</body>
</html>