-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-08-2016 a las 18:25:04
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `smdr`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `contrasena` varchar(100) NOT NULL,
  `Nombre` text NOT NULL,
  `Paterno` text NOT NULL,
  `Materno` text NOT NULL,
  `Empresa` text NOT NULL,
  `Departamento` text NOT NULL,
  `deptoTari` varchar(60) NOT NULL,
  `NumTlefonico` text NOT NULL,
  `Ext` varchar(250) NOT NULL,
  `Email` varchar(250) NOT NULL,
  `tipoUsuario` varchar(100) NOT NULL DEFAULT 'Estandar',
  `estatus` varchar(100) NOT NULL DEFAULT 'Activo',
  `AuthCode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `contrasena`, `Nombre`, `Paterno`, `Materno`, `Empresa`, `Departamento`, `deptoTari`, `NumTlefonico`, `Ext`, `Email`, `tipoUsuario`, `estatus`, `AuthCode`) VALUES
(1, 'admin', 'Ginsa200', 'Administrador', 'Aministrador', 'Administrador', 'GINSA', 'Sistemas', '', '0', '0', 'sistemas@ginsa.commx', 'admin', 'Activo', 0),
(2, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOSE DE JESUS', 'HERNANDEZ', 'MARTINEZ', 'GINSA', 'SISTEMAS', 'ADMINISTRACION', '222 282 64 00', '4595', 'sistemas@ginsa.com.mx', 'Estandar', 'Activo', 0),
(3, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ALEJANDRO', 'DE JESUS', 'MARTINEZ', 'GINSA', 'SISTEMAS', 'ADMINISTRACION', '222 282 64 00', '4595', 'sistemas@ginsa.com.mx', 'Estandar', 'Activo', 0),
(4, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ROBERTO', 'AGUILAR', 'MORALES', 'GINSA', 'GERENTE DE SISTEMAS', 'SISTEMAS', '222 282 64 00', '4594', 'sistemas@ginsa.com.mx', 'Estandar', 'Activo', 2788),
(6, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JAQUELINE', 'MARTINEZ', 'GARCÃA', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SEMINUEVOS | HOSTESS', '', '222 228 54 78 | 222 228 62 90 ', '', 'recepcion.seminuevos@ginsa.com.mx', 'Estandar', 'Activo', 0),
(11, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'GM', 'FLEET', 'SERVICES', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'FLOTILLAS', '', '222 225 92 60', '.', '.', 'Estandar', 'Activo', 0),
(13, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JAIME', 'GARCIA', 'BAUTISTA', 'GINSA', 'DIRECCION | DIRECTOR GENERAL', 'ADMINISTRACION', '222 240 21 00', '4090', 'jaimegb@ginsa.com.mx', 'Estandar', 'Activo', 0),
(15, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'RODRIGO', 'GARCIA', 'CIANCA', 'PARISAUTO DE PUEBLAS.A. DE C.V.', 'GERENTE OPERATIVO', 'ADMINISTRACION', '222 246 46 66', '4200', 'rodrigo.garcia@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(17, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'IGNACIO ', 'ORTIZ ', 'MARTINEZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SERVICIO | JEFE DE TALLER', 'SERVICIO', '222 240 00 21', '4038', 'ignacio.ortiz@ginsa.com.mx ', 'Estandar', 'Activo', 1612),
(18, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JAIME', 'GARCIA ', 'CALDERON', 'GINSA', 'PRESIDENTE', '', '222 249 56 56', '.', 'jgarcia@ginsa.com.mx', 'Estandar', 'Activo', 0),
(19, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', ' JOSE GERARDO ', 'FRIAS', 'GARZA', 'GINSA', 'ADMINISTRATIVO', '', '222 249 56 56', '.', 'gerardo.frias@ginsa.com.mx', 'Estandar', 'Activo', 0),
(20, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOSE DE JESUS', 'SANDOVAL ', 'CARRANZA', 'GINSA', 'RECEPCION ', 'ADMINISTRACION', '222 249 56 46', '321', 'S/correo', 'Estandar', 'Inactivo', 0),
(21, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOSE GENARO', 'GARCIA ', 'Y GARCIA', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'GERENTE DE FLOTILLAS', 'FLOTILLAS', '2222655261', '4551', 'genaro.garcia@ginsa.com.mx', 'Estandar', 'Activo', 2390),
(22, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'LUCILA', 'PEREZ', 'VALENCIA', 'CONVERSIONES GARCIA PINEDA S.A DE C.V', 'ADMINISTRACION / ASISTENTE GTE. FLOTILLAS', 'VENTAS', '222 282 64 00', '4556', 'lucila.perez@ginsa.com.mx', 'Estandar', 'Activo', 0),
(23, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'CESAR AUGUSTO', 'ARIAS', 'SANDOVAL', 'CONVERSIONES GARCIA PINEDA S.A DE C.V', 'GERENTE ADMINISTRATIVO', 'CONTACT CENTER', '222 282 64 00', '4520', 'cesar.arias@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(24, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'CESAR RAFAEL', 'AZPIROZ', 'ORTEGA', 'CONVERSIONES GARCIA PINEDA S.A DE C.V', 'CREDITO Y COBRANZA', 'ADMINISTRACION', '222 282 64 00', '4512', 'cesar.azpiroz@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(25, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'OLGA', 'GUTIERREZ ', 'CARCAMO', 'CONVERSIONES GARCIA PINEDA S.A DE C.V', 'VENTAS', 'FLOTILLAS', '222 282 64 00', '4559', 'olga.gutierrez@ginsa.com.mx', 'Estandar', 'Activo', 1332),
(26, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARÃA DE LA LUZ', 'CHAVEZ ', 'CONTRERAS', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'DIRECCIÃ“N | APOYO A DIR. GRAL. Y ADMON. DE VTAS.', 'VENTAS', '222 240 00 21', '4053', 'maryluz.chavez@ginsa.com.mx', 'Estandar', 'Activo', 3400),
(27, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'FERNANDO NICOLÃS', 'SALAS', 'SIERRA', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'GERENTE GENERAL', 'ADMINISTRACION', '044 2224029047', '4000', 'fernando.salas@ginsa.com.mx', 'Estandar', 'Activo', 2590),
(28, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'DIANA PATRICIA', 'SORIANO', 'REYES', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | JEFE DE PISO ', 'VENTAS', '222 240 00 21', '4050', '', 'Estandar', 'Inactivo', 0),
(29, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'VERONICA', 'ROMERO ', 'LOPEZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'GERENTE ADMINISTRATIVO', 'ADMINISTRACION', '222 240 00 21', '4002', 'veronica.romero@ginsa.com.mx', 'Estandar', 'Activo', 9317),
(30, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'THALIA', 'GUZMAN', 'MARTAGON', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'ADMINISTRACIÃ“N | AUXILIAR CONTABLE', 'ADMINISTRACION', '222 240 00 21', '4005', 'thalia.guzman@ginsa.com.mx', 'Estandar', 'Activo', 1151),
(31, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ANDRES', 'ANDRADE', 'OLLARZABAL', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'ADMINISTRACIÃ“N | AUXILIAR CONTABLLE', 'ADMINISTRACION', '222 240 00 21', '4004', 'andres.andrade@ginsa.com.mx', 'Estandar', 'Activo', 1415),
(32, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MONTSERRAT ', 'DEL VALLE', 'MUÃ‘OZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | PROMOCIÃ“N DE CRÃ‰DITO Y FACTURACIÃ“N', 'VENTAS', '222 240 00 21', '4056', 'montserrat.delvalle@ginsa.com.mx', 'Estandar', 'Activo', 7821),
(33, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MIYUKI', 'CRESPO', 'LEZA', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | AUXILIAR DE RESPONSE', 'VENTAS', '222 240 00 21', '4057', 'miyuki.crespo@insa.com.mx', 'Estandar', 'Activo', 1247),
(34, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JORGE ARTURO', 'HERNÃNDEZ ', 'CARVANTES', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'ADMINISTRACIÃ“N | GESTOR ADMINISTRATIVO', 'VENTAS', '222 581 14 29', '4055', 'jorge.hernandez@ginsa.com.mx', 'Estandar', 'Activo', 1015),
(35, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'KARINA NEIL ', 'CADENA', 'MENDOZA', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'ADMINISTRACIÃ“N | CAJERA', 'ADMINISTRACION', '222 240 00 21', '4006', 'caja.angelopolis@ginsa.com.mx', 'Estandar', 'Inactivo', 5006),
(36, 'Ivonne', 'Solar123', 'LAURA IVONNE', 'SOLAR', 'DÃAZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'GERENTE RECURSOS HUMANOS / LIDER DE CALIDAD', '', '222 240 00 21', '4001', 'ivonne.solar@ginsa.com.mx', 'admin', 'Activo', 0),
(37, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MONTSERRAT', 'FRAUSTRO', 'NEGRETE', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'GERENTE DE MERCADOTECNIA', 'MERCADOTECNIA', '222 240 00 21', '4093', 'monserrat.fraustro@ginsa.com.mx', 'Estandar', 'Activo', 4835),
(38, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOSE EDUARDO', 'GUTIERREZ ', 'VIVEROS', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | CONTROL DE UNIDADES NUEVAS', 'VENTAS', '222 240 00 21', '4054', 'unidades.nuevas@ginsa.com.mx', 'Estandar', 'Activo', 2734),
(39, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'SERGIO', 'CASTILLO', 'MORALES', 'PARISAUTO DE PUEBLA', 'GERENTE', 'ADMINISTRACION', '222 246 46 66', '4200', 'sergio.castillo@ginsa.com.mx', 'Estandar', 'Activo', 9021),
(40, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARIA ELENA', 'GARCÃA', 'JIMENEZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SERVICIO / ASISTENTE GTE DE SERVICIO', 'SERVICIO', '222 240 00 21', '4031', 'mariaelena.garcia@ginsa.com.mx', 'Estandar', 'Activo', 3004),
(41, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARCOS', 'HERNANDEZ', 'SANCHEZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SERVICIO | ADMINISTRATIVO DE GARANTÃAS', 'SERVICIO', '222 240 00 21', '4037', '304garantiasgpa@ginsa.com.mx', 'Estandar', 'Activo', 6742),
(42, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ALBERTO', 'LEGASPI', 'FUENTES', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SEMINUEVOS | VALUADOR DE AUTOS', 'VENTAS', '222 228 54 78', '4060', 'alberto.legaspi@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(43, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ARTURO', 'CASTILLO', 'SÃNCHEZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SERVICIO | ASESOR DE SERVICIO', 'SERVICIO', '222 240 00 21', '4033', 'arturo.castillo@ginsa.com.mx', 'Estandar', 'Activo', 1276),
(44, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'KARINA', 'ROMERO', 'LOPEZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SERVICIO | ASESORA DE SERVICIO', 'SERVICIO', '222 240 00 21', '4036', 'karina.romero@ginsa.com.mx', 'Estandar', 'Activo', 8042),
(45, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARIA ENRIQUETA ', 'REYES ', 'MARTÃNEZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SERVICIO | ASESORA DE SERVICIO', 'SERVICIO', '222 240 00 21', '4034', 'enriqueta@ginsa.com.mx', 'Estandar', 'Activo', 1090),
(46, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ALBERT', 'LUNA', 'CASTELAN', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SERVICIO | ASESOR DE SERVICIO', 'SERVICIO', '222 240 00 21', '4035', 'albert.luna@ginsa.com.mx', 'Estandar', 'Activo', 2894),
(47, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'LORELEY CARIME', 'RAMIREZ', 'HERNANDEZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'GERENTE DE REFACCIONES', 'REFACCIONES', '222 240 00 21', '4040', 'loreley.ramirez@ginsa.com.mx', 'Estandar', 'Activo', 8041),
(48, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'LUIS', 'DOMINGUEZ', 'RAMIREZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'REFACCIONES | VTAS. MOSTRADOR', 'REFACCIONES', '222 240 00 21', '4043', 'mostradorgpa@ginsa.com.mx', 'Estandar', 'Activo', 7102),
(49, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'EDUARDO', 'MORALES', 'CONTRERAS', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'REFACCIONES | VTAS. COLISIÃ“N', 'REFACCIONES', '222 240 00 21', '4041', 'colision.gpa@ginsa.com.mx', 'Estandar', 'Activo', 0),
(50, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARIA SONIA ', 'MEYO', 'FLORES ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'REFACCIONES | ENCARGADO DE ALMACÃ‰N', 'REFACCIONES', '222 240 00 21', '4041', 'almacen.gpa@ginsa.com.mx', 'Estandar', 'Activo', 0),
(51, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ANAHY', 'MENDOZA', 'MUÃ‘OZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SUPERV. DE CONTACT CENTER', 'BDC POSTVENTA', '222 225 92 90', '4020', 'anahy.mendoza@ginsa.com.mx', 'Estandar', 'Activo', 2212),
(52, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'LIZETH', 'CAMARILLO', 'SERRANO', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'TELEMARKETING | ASESOR TELEFÃ“NICO-VEHICULOS NVOS', 'BDC POSTVENTA', '222 240 00 21', '4026', 'lizeth.camarillo@ginsa.com.mx', 'Estandar', 'Activo', 679),
(53, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'LORENA', 'CERON', 'LOPEZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'CONTACT CENTER | ASESOR TEL.', 'BDC POSTVENTA', '222 240 00 21', '4021', 'contact.center3@ginsa.com.mx', 'Estandar', 'Activo', 2041),
(54, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'YUNUEN', 'TELLEZ', 'TORRES', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'TELEMARKETING | ASESOR TELEFÃ“NICO-VEHICULOS NVOS', 'BDC POSTVENTA', '222 240 00 21', '4027', 'yunuen.tellez@ginsa.com.mx', 'Estandar', 'Activo', 1478),
(55, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MIGUEL', 'GARCIA', 'ZAYAS', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'CONTACT CENTER | ASESOR TEL.', 'BDC POSTVENTA', '222 240 00 21', '4023', 'contact.center2@ginsa.com.mx', 'Estandar', 'Activo', 7602),
(56, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'LUCIA', 'VARGAS', 'GONZALEZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'CONTACT CENTER | ASESOR TEL.', 'BDC POSTVENTA', '222 240 00 21', '4021', 'contactcenter5@ginsa.com.mx', 'Estandar', 'Activo', 7294),
(57, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'RAQUEL', 'GARCIA', 'CRUZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SEGUROS | ASESOR DE SEGUROS GP', 'SEGUROS', '222 337 28 88', '4007', 'seguros.gpa@ginsa.com.mx', 'Estandar', 'Activo', 3813),
(58, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARIEL', 'LEAL', 'CORDOVA', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 240 00 21', '4062', 'mariel.leal@ginsa.com.mx', 'Estandar', 'Activo', 8013),
(59, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'STEFFANIA', 'GONZALEZ', 'NIELSEN', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | HOSTESS - MAÃ‘ANA', 'VENTAS', '222 240 00 21', '4050', 'recepciongpa@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(60, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'CECILIA', 'CÃRDENAS', 'DE LA CERDA', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | GERENTE DE NEGOCIOS F&I', 'VENTAS', '222 650 35 85', '4052', 'cecilia.cardenas@ginsa.com.mx', 'Estandar', 'Activo', 891),
(61, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'SONIA MONTSERRAT', 'HUITZIL', 'RUIZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 174 15 99', '4068', 'sonia.huitzil@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(63, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'KARLA LORENA', 'NOLASCO', 'VALERA', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 709 60 23', '4063', 'karla.nolasco@ginsa.com.mx', 'Estandar', 'Activo', 4592),
(64, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'RUBEN ALBERTO', 'CORONA', 'CANO', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 277 93 72', '4064', 'ruben.corona@ginsa.com.mx', 'Estandar', 'Activo', 7120),
(66, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'AGUSTÃN ABRAHAM', 'HERRERA', 'LUNA', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 198 38 88', '4065', 'agustin.herrera@ginsa.com.mx', 'Estandar', 'Activo', 2048),
(67, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'NAYMA', 'AGUILAR', 'ALONSO', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 380 32 50', '4066', 'nayma.aguilar@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(68, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'EFRAIN', 'REBOLLEDO', 'STEFFANONI', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | GERENTE DE VENTAS', 'VENTAS', '222 240 00 21', '4051', 'efrain.rebolledo@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(70, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARIA GRISEL', 'GARCIA', 'MARQUEZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 176 67 16', '4067', 'grisel.garcia@ginsa.com.mx', 'Estandar', 'Activo', 1873),
(71, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'CARLOS ALBERTO', 'RUGERIO', 'SPECIA', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS', 'VENTAS', '222 453 32 11 / 222 817 92 29', '4069', 'carlos.rugerio@ginsa.com.mx', 'Estandar', 'Activo', 3526),
(72, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'LUIS EDUARDO', 'VALVERDE', 'SALAZAR', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VTAS. FORÃNEAS | ASESOR DE VTAS. FORÃNEAS', 'VENTAS', '222 584 12 56', '4072, 4073, 4074', 'luis.valverde@ginsa.com.mx', 'Estandar', 'Inactivo', 6521),
(73, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOSE ANTONIO', 'CABAÃ‘AS', 'LOPEZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VTAS. FORÃNEAS | ASESOR DE VTAS. FORÃNEAS', 'VENTAS', '222 965 34 55', '4072, 4073, 4074', 'jose.cabanas@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(74, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ANDRES', 'ZACA', 'NAYOTL', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VTAS. FORÃNEAS | ASESOR DE VTAS. FORÃNEAS', 'VENTAS', '222 200 41 92', '4072, 4073, 4074', 'andres.zaca@ginsa.com.mx', 'Estandar', 'Activo', 7510),
(75, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARTIN ALFREDO', 'OCHOA', 'OROS', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 378 67 81', '4078', 'martin.ochoa@ginsa.com.mx', 'Estandar', 'Activo', 9720),
(77, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'OMAR', 'GOMEZ', 'MARTINEZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'Ventas | ASESOR DE VTAS. LEADIT', 'VENTAS', '222 682 02 83', '4066', 'omar.gomez@ginsa.com.mx', 'Estandar', 'Activo', 0),
(78, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'RAFAEL', 'ZAVALA', 'CRUZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 725 64 32', '4061', 'rafael.zavala@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(79, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'STHEPHANY CECILIA', 'VILLAGOMEZ', 'UGALDE', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 511 19 64', '4070', 'sthephany.villagomez@ginsa.com.mx', 'Estandar', 'Activo', 4644),
(80, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'FRANCISCO FELIPE', 'TORRES', 'REYES', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VTAS. FORÃNEAS | ASESOR DE VTAS. FORÃNEAS', 'VENTAS', '222 507 69 36', '4072, 4073, 4074', 'felipe.torres@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(81, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOSE DAVID HIGINIO', 'ROJAS', 'LIMA', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ASESOR DE VTAS. SUAUTO', '', '222 185 09 35', '', 'david.rojas@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(82, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ALEJANDRA', 'GARCIA', 'BAUTISTA', 'MAQUINARIA PESADA GACIA PINEDA S.A DE C.V', 'GERENTE ADMINISTRATIVO', 'ADMINISTRACION', '222 224 95 92', '230', 'agarcia@ytopuebla.com.mx', 'Estandar', 'Activo', 0),
(83, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JORGE', 'DE LA ROZA', 'EZPARZA', 'MAQUINARIA PESADA GACIA PINEDA S.A DE C.V', 'GERENTE OPERATIVO', 'ADMINISTRACION', '222 224 95 94', '253', 'jrosa@ytopuebla.com.mx', 'Estandar', 'Inactivo', 0),
(84, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'CESAR', 'GALICIA', 'FLORES', 'MAQUINARIA PESADA GACIA PINEDA S.A DE C.V', 'ASISTENTE ADMINISTRATIVO', '', '222 224 95 93', '.', 'cesargalicia@ytopuebla.com.mx', 'Estandar', 'Inactivo', 0),
(85, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ALEJANDRA', 'ROLDAN', 'HERNANDEZ', 'PARISAUTO DE PUEBLA S.A DE C.V', 'DIRECCIÃ“N | APOYO ADMTIVO. DE GCIA. OPER. Y VENTAS', 'VENTAS', '222 246 46 66', '4253', 'alejandra.roldan@ginsa.com.mx', 'Estandar', 'Activo', 2324),
(86, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ROSA MARIA ', 'LARGAESPADA', 'BAGATELLA', 'PARISAUTO DE PUEBLA S.A DE C.V', 'GERENTE DE SEGUROS', 'SEGUROS', '222 246 46 66', '4292', 'rossy.largaespada@ginsa.com.mx', 'Estandar', 'Activo', 1246),
(87, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JESUS', 'GOMEZ', 'CEDILLO', 'PARISAUTO DE PUEBLA S.A DE C.V', 'ADMINISTRACION | GERENTE DE RECURSOS HUMANOS', 'ADMINISTRACION', '222 246 46 66', '4201', 'jesus.gomez@ginsa.com.mx', 'Estandar', 'Activo', 1015),
(88, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'HORACIO LOPEZ', 'SIERRA', 'ALCÃNTARA', 'PARISAUTO DE PUEBLA S.A DE C.V', 'GERENTE ADMINISTRATIVO', 'ADMINISTRACION', '222 246 46 66', '4202', 'horacio.lopez@ginsa.com.mx', 'Estandar', 'Activo', 0),
(89, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARIA DE GUADALUPE', 'MIÃ‘ON', 'ARELLANO', 'PARISAUTO DE PUEBLA S.A DE C.V', 'ADMINISTRACIÃ“N | AUXILIAR DE CONTABILIDAD', 'ADMINISTRACION', '222 246 46 66', '4203', 'guadalupe.mignon@ginsa.com.mx', 'Estandar', 'Activo', 7765),
(90, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'KARINA', 'TORRES', 'BAUTISTA', 'PARISAUTO DE PUEBLA S.A DE C.V', 'ADMINISTRACIÃ“N | AUXILIAR DE CONTABILIDAD', 'ADMINISTRACION', '222 246 46 66', '4205', 'karina.torres@ginsa.com.mx', 'Estandar', 'Activo', 2907),
(91, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARIA DE LA PAZ', 'FERNANDEZ', 'RODRIGUEZ', 'PARISAUTO DE PUEBLA S.A DE C.V', 'ADMINISTRACIÃ“N | AUXILIAR DE CONTABILIDAD', 'ADMINISTRACION', '222 246 46 66', '4204', 'mariadelapaz.fernandez@ginsa.com.mx', 'Estandar', 'Activo', 1832),
(92, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARIA DEL CARMEN', 'ARELLANO', 'VIVEROS', 'GINSA', 'TESORERA', '', '222 249 56 56', '-', 'carmen.arellano@ginsa.com.mx', 'Estandar', 'Activo', 0),
(93, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'GLORIA', 'ALEMAN', 'TINAJERO', 'PARISAUTO DE PUEBLA S.A DE C.V', 'ADMINISTRACIÃ“N | RESPONSABLE DE CAJA', 'ADMINISTRACION', '222 246 46 66', '4206', 'caja.parisauto.puebla@ginsa.com.mx', 'Estandar', 'Activo', 1952),
(94, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MONTSERRAT', 'FRAUSTRO', 'NEGRETE', 'PARISAUTO DE PUEBLA S.A DE C.V', 'GERENTE DE MERCADOTECNIA', 'MERCADOTECNIA', '222 246 46 66', '4293', 'monserrat.fraustro@ginsa.com.mx', 'Estandar', 'Activo', 4835),
(95, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ELIZABETH', 'JIMENEZ', '', 'PARISAUTO DE PUEBLA S.A DE C.V', 'MERCADOTECNIA | RESPONSABLE VN MARKETING', 'CONTACT CENTER', '222 246 46 66', '4222', 'contactovn.parisauto@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(96, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ALEJANDRO ', 'VELEZ', 'ORTEGA', 'PARISAUTO DE PUEBLA S.A DE C.V', 'MERCADOTECNIA | RESPONSABLE VN MARKETING', 'CONTACT CENTER', '222 246 46 66', '4222', 'contactovn.parisauto@ginsa.com.mx', 'Estandar', 'Inactivo', 4567),
(97, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MONICA', 'VELEZ', 'HERNANDEZ', 'PARISAUTO DE PUEBLA S.A DE C.V', 'HOSTESS MAÃ‘ANA', 'VENTAS', '222 246 46 66', '4250', 'recepcion.puebla@ginsa.com.mx', 'Estandar', 'Inactivo', 6011),
(98, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'PAOLA', 'BARRANCO', 'LOPEZ', 'PARISAUTO DE PUEBLA S.A DE C.V', 'HOSTESS TARDE', 'VENTAS', '222 246 46 66', '4250', 'recepcion.puebla@ginsa.com.mx', 'Estandar', 'Inactivo', 1056),
(99, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'FARY', 'CAPRILES', 'BERROS', 'PARISAUTO DE PUEBLA S.A DE C.V', 'GERENTE DE REFACCIONES', 'REFACCIONES', '222 246 46 66', '4240', 'fary.capriles@ginsa.com.mx', 'Estandar', 'Activo', 7301),
(100, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MIGUEL', 'CHAVEZ', 'PADILLA', 'PARISAUTO DE PUEBLA S.A DE C.V', 'VENTAS COLISION', 'REFACCIONES', '222 246 46 66', '4241', 'colision.pap@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(101, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ABIGAIL', 'GARCIA', 'CRUZ', 'PARISAUTO DE PUEBLA S.A DE C.V', 'REFACCIONES | VTAS. TALLER', 'REFACCIONES', '222 246 46 66', '4243', 'refac.parisauto.puebla@ginsa.com.mx', 'Estandar', 'Activo', 2946),
(102, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOSE ALBERTO', 'MORALES', 'LIMA', 'PARISAUTO DE PUEBLA S.A DE C.V', 'REFACCIONES / MOSTRADOR', 'REFACCIONES', '222 246 46 66', '4242', 'mostrador.pap@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(103, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'OMAR', 'ZACARIAS', 'NOLASCO', 'PARISAUTO DE PUEBLA S.A DE C.V', 'GERENTE DE SERVICIO', 'SERVICIO', '222 246 46 66', '4230', 'omar.zacarias@ginsa.com.mx', 'Estandar', 'Activo', 8632),
(104, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARISSA', 'SOTO', 'REYES', 'PARISAUTO DE PUEBLA S.A DE C.V', 'ASISTENTE GERENCIA DE SERVICIO', 'SERVICIO', '222 282 64 00', '4231', 'asistger.serpue@ginsa.com.mx', 'Estandar', 'Inactivo', 4681),
(105, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'FERNANDO', 'RIVERA', 'AGUILAR', 'PARISAUTO DE PUEBLA S.A DE C.V', 'CONSEJERO TECNICO', 'SERVICIO', '222 246 46 66', '4238', 'consejero.pue@ginsa.com.mx', 'Estandar', 'Activo', 1583),
(106, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JAIME', 'PORRAS', 'MARIN', 'PARISAUTO DE PUEBLA S.A DE C.V', 'ASESOR DE SERVICIO', 'SERVICIO', '222 246 46 66', '4233', 'asesorpp@ginsa.com.mx', 'Estandar', 'Activo', 6020),
(107, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'DAVID', 'SILVA', 'CASARRUBIAS', 'PARISAUTO DE PUEBLA S.A DE C.V', 'ASESOR DE SERVICIO', 'SERVICIO', '222 246 46 66', '4234', 'asesorpp1@ginsa.com.mx', 'Estandar', 'Activo', 5120),
(108, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ILLIANA', 'CALDERON', 'SANCHEZ', 'PARISAUTO DE PUEBLA S.A DE C.V', 'SERVICIO | RESPONSABLE DE CALIDAD', 'SERVICIO', '222 246 46 66', '', 'calidad.ppu@ginsa.com.mx', 'Estandar', 'Inactivo', 1207),
(109, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'NALLELY', 'MUÃ‘OZ', 'PEREZ', 'PARISAUTO DE PUEBLA S.A DE C.V', 'ASESOR DE CITAS', 'CONTACT CENTER', '222 246 46 66', '4221', 'citas.servicio@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(110, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'KARLA ERENDIRA', 'AVENDAÃ‘O', 'LOPEZ', 'PARISAUTO DE PUEBLA S.A DE C.V', 'VENTAS | ENCARGADO DE VEHICULOS NUEVOS', 'VENTAS', '222 246 46 66', '4254', 'entregasvn.parisauto.pue@ginsa.com.mx', 'Estandar', 'Activo', 1624),
(111, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JANETH', 'PACHECO', 'SOSA', 'PARISAUTO DE PUEBLA S.A DE C.V', 'PROMOCIÃ“N DE CRÃ‰DITO F&I', 'VENTAS', '222 246 46 66', '4252', 'janeth.pacheco@ginsa.com.mx', 'Estandar', 'Activo', 3032),
(112, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'YESENIA', 'HERNANDEZ', 'CRUZ', 'PARISAUTO DE PUEBLA S.A DE C.V', 'SEGUROS | ASESOR DE SEGUROS', 'SEGUROS', '222 246 46 66', '4207', 'seguros.parispue@ginsa.com.mx', 'Estandar', 'Activo', 1515),
(113, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'CESAR', 'LINARTE', 'GONZALEZ', 'PARISAUTO DE PUEBLA S.A DE C.V', 'ASESOR DE VENTAS', 'VENTAS', '222 246 46 66', '4262', 'cesa.linarte@ginsa.com.mx', 'Estandar', 'Activo', 0),
(115, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'CHRISTIAN ALBERTO', 'MENDEZ', 'CORNEJO', 'PARISAUTO DE PUEBLA S.A DE C.V', 'GERENTE DE VENTAS', 'VENTAS', '222 246 46 66', '4251', 'christian.mendez@ginsa.com.mx', 'Estandar', 'Activo', 2314),
(116, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'SILVIA AURORA', 'CARRILLO', 'SUÃREZ', 'PARISAUTO DE PUEBLA S.A DE C.V', 'ASESOR DE VENTAS', 'VENTAS', '222 246 46 66', '4263', 'silvia.carrillo@ginsa.com.mx', 'Estandar', 'Activo', 8906),
(117, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'GUSTAVO', 'CHEVALIER', 'PACHECO', 'PARISAUTO DE PUEBLA S.A DE C.V', 'ASESOR DE VENTAS', 'VENTAS', '222 246 46 66', '4261', 'gustavo.chevalier@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(118, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'PATRICIA ALEJANDRA', 'SÃNCHEZ', 'FLORES', 'PARISAUTO DE PUEBLA S.A DE C.V', 'ASESOR DE VENTAS', 'VENTAS', '222 246 46 66', '4263', 'patricia.sanchez@ginsa.com.mx', 'Estandar', 'Activo', 2256),
(119, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'AGUSTIN ALBERTO', 'RAMIREZ', 'AGUILAR', 'PARISAUTO DE PUEBLA S.A DE C.V', 'ASESOR DE VENTAS', 'VENTAS', '222 246 46 66', '4265', 'alberto.ramirez@ginsa.com.mx', 'Estandar', 'Activo', 2064),
(120, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOSE RAMON', 'RODRIGUEZ', 'GUERRERO', 'PARISAUTO DE PUEBLA S.A DE C.V', 'ASESOR DE VENTAS', 'VENTAS', '222 246 46 66', '4264', 'ramon.rodriguez@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(121, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'GEORGINA', 'GARCIA', 'RAMIREZ', 'PARISAUTO DE PUEBLA S.A DE C.V', 'ASESOR DE VENTAS', 'VENTAS', '222 246 46 66', '4265', 'georgina.garcia@ginsa.com.mx', 'Estandar', 'Activo', 8720),
(122, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'FERNANDO EDUARDO', 'MEJIA', 'ARROYO', 'PARISAUTO DE SATELITE S.A DE C.V', 'GERENTE OPERATIVO', 'ADMINISTRACION', '555 393 17 28', '4600', 'fernando.mejia@ginsa.com.mx', 'Estandar', 'Activo', 1007),
(123, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARIA DE GUADALUPE', 'SALAZAR', 'NIEVES', 'PARISAUTO DE SATELITE S.A DE C.V', 'AUXILIAR DE CONTABILIDAD', 'ADMINISTRACION', '555 393 17 28', '4603', 'contabilidad.psat@ginsa.com.mx', 'Estandar', 'Activo', 6160),
(124, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARIBEL', 'ANGELES', 'VAZQUEZ', 'PARISAUTO DE SATELITE S.A DE C.V', 'RESPONSABLE DE CAJA', 'ADMINISTRACION', '555 393 17 28', '4606', 'caja.satelite@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(125, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'DAPHNE DANAE', 'LOZA', 'CÃRDENAS', 'PARISAUTO DE SATELITE S.A DE C.V', 'PROMOCIÃ“N DE CRÃ‰DITO F&I', 'VENTAS', '555 393 17 28', '4652', 'dapnhe.loza@ginsa.com.mx', 'Estandar', 'Activo', 4650),
(127, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'LILIANA', 'AGUILAR', 'DAVILA', 'PARISAUTO DE SATELITE S.A DE C.V', 'HOSTESS', 'VENTAS', '555 393 17 28', '4650', 'recepcion.satelite@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(128, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'EDUARDO ABEL', 'LORENZO', 'ROSETE', 'PARISAUTO DE SATELITE S.A DE C.V', 'GERENTE DE REFACCIONES', 'REFACCIONES', '555 393 17 28', '4640', 'eduardo.lorenzo@ginsa.com.mx', 'Estandar', 'Activo', 9742),
(129, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'FERNANDO', 'RIVAS', 'SERRANO', 'PARISAUTO DE SATELITE S.A DE C.V', 'SERVICIO | ASESOR DE SERVICIO', 'SERVICIO', '555 393 17 28', '4633', 'fernando.rivas@ginsa.com.mx', 'Estandar', 'Activo', 5031),
(130, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARCO ANTONIO', 'HERNÃNDEZ', 'CHÃVEZ', 'PARISAUTO DE SATELITE S.A DE C.V', 'REFACCIONES | VENTAS MOSTRADOR', 'REFACCIONES', '555 393 17 28', '4642', 'marco.hernandez@ginsa.com.mx', 'Estandar', 'Activo', 509),
(131, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ENRIQUE', 'GONZALEZ', 'PEREZ', 'PARISAUTO DE SATELITE S.A DE C.V', 'GERENTE DE SERVICIO', 'SERVICIO', '555 393 17 28', '4630', 'enrique.gonzalez@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(132, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MONSERRAT ANAIZA', 'GARIBAY', 'ALVAREZ', 'PARISAUTO DE SATELITE S.A DE C.V', 'ASISTENTE DE GTE. SERVICIO Y GARANTÃAS', 'SERVICIO', '555 393 17 28', '4631', 'monserrat.garibay@ginsa.com.mx', 'Estandar', 'Activo', 8192),
(133, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'OSCAR', 'SANCHEZ', 'LUNA', 'PARISAUTO DE SATELITE S.A DE C.V', 'ASESOR DE SERVICIO', 'SERVICIO', '555 393 17 28', '4634', 'oscar.sanchez@ginsa.com.mx', 'Estandar', 'iNACTIVO', 0),
(134, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'KARLA GUADALUPE', 'GAITAN', 'GARCÃA', 'PARISAUTO DE SATELITE S.A DE C.V', 'SERVICIO | ASESOR DE CITAS', 'SERVICIO', '555 393 17 28', '4632', 'citas.parisauto.satelite@ginsa.com.mx', 'Estandar', 'Activo', 8131),
(135, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MANUEL', 'DE LA ROSA', 'LIRA', 'PARISAUTO DE SATELITE S.A DE C.V', 'GERENTE DE SERVICIO', 'SERVICIO', '555 393 17 28', '4630', 'manuel.delarosa@ginsa.com.mx', 'Estandar', 'Activo', 5118),
(136, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'LEONARDO', 'ACOSTA', 'ORTIZ', 'PARISAUTO DE SATELITE S.A DE C.V', 'JEFE DE HOJATERIA  Y PINTURA', 'SERVICIO', '555 393 17 28', '4635', 'hypsatelite@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(137, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'EDMUNDO', 'DOMINGUEZ', 'LOPEZ', 'PARISAUTO DE SATELITE S.A DE C.V', 'SERVICIO | CONSEJERO TECNICO', 'SERVICIO', '555 393 17 28', '4638', 'consejero.sat@ginsa.com.mx', 'Estandar', 'Activo', 2204),
(138, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'OMAR ARMANDO', 'CORNEJO', 'BECERRA', 'PARISAUTO DE SATELITE S.A DE C.V', 'SERVICIO | RESPONSABLE DE CALIDAD', 'SERVICIO', '555 393 17 28', '', 'calidad.psa@ginsa.com.mx', 'Estandar', 'Activo', 0),
(139, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'EVA ALICIA', 'JIMÃ‰NEZ', 'SÃNCHEZ', 'PARISAUTO DE SATELITE S.A DE C.V', 'ASESOR DE VENTAS', 'VENTAS', '555 393 17 28', '4661, 4662, 4663, 4665', 'alicia.jimenez@ginsa.com.mx', 'Estandar', 'Activo', 2215),
(140, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'CARLOS DAVID', 'CARRILLO', 'GONZALEZ', 'PARISAUTO DE SATELITE S.A DE C.V', 'ASESOR DE VENTAS', 'REFACCIONES', '555 393 17 28', '4643', 'david.carrillo@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(141, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'CARLOS EDUARDO', 'MINGUER', 'GRANADOS', 'PARISAUTO DE SATELITE S.A DE C.V', 'ASESOR DE VENTAS', 'VENTAS', '555 393 17 28', '4661, 4662, 4663, 4665', 'carlos.minguer@ginsa.com.mx', 'Estandar', 'Activo', 4565),
(142, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'EDGAR', 'TELLEZ', 'ZEPEDA', 'PARISAUTO DE SATELITE S.A DE C.V', 'ASESOR DE VENTAS', 'VENTAS', '555 393 17 28', '4661, 4662, 4663, 4665', 'edgar.tellez@ginsa.com.mx', 'Estandar', 'Activo', 2054),
(143, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ARTURO GILBERTO', 'AMADOR', 'GARCIA', 'PARISAUTO DE SATELITE S.A. DE C.V.', 'ASESOR DE VENTAS', 'VENTAS', '555 393 17 28', '4661, 4662, 4663, 4665', 'gilberto.amador@ginsa.com.mx', 'Estandar', 'Activo', 4655),
(144, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'VIANCA NYDIA', 'ALVARADO', 'URBANO', 'PARISAUTO DE SATELITE S.A DE C.V', 'ASESOR DE SEGUROS', 'ADMINISTRACION', '555 393 17 28', '4607', 'vianca.alvarado@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(145, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'GERARDO', 'ALFARO', 'ERDING', 'PARISAUTO DE SATELITE S.A DE C.V', 'ASESOR DE VENTAS', 'VENTAS', '555 393 17 28', '4661, 4662, 4663, 4665', 'gerardo.alfaro@ginsa.com.mx', 'Estandar', 'Activo', 0),
(146, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ARTURO', 'ROMERO', 'LOPEZ', 'PARISAUTO DE SATELITE S.A DE C.V', 'ASESOR DE VENTAS', 'VENTAS', '555 393 17 28', '4661, 4662, 4663, 4665', ' arturo.romero@ginsa.com.mx', 'Estandar', 'Activo', 0),
(147, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'PEDRO ', 'RODRIGUEZ ', 'DE LA TORRE', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SEMINUEVOS | GERENTE SEMINUEVOS', '', '222 228 5478 | 222 228 62 90', '', ' pedro.rodriguez@ginsa.com.mx', 'Estandar', 'Activo', 0),
(148, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOSE ANTONIO', 'CASTILLO', 'GOMEZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SEMINUEVOS | VALUADOR DE AUTOS', '', '222 228 54 78', '', 'jose.castillo@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(149, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'HECTOR ALBERTO ', 'MARQUEZ ', 'ACOSTA', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SEMINUEVOS | ASESOR DE VENTAS', '', '222 228 54 78 | 222 707 47 77', '', 'hector.marquez@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(150, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ALFREDO', 'PÃ‰REZ', 'CARRILLO', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SEMINUEVOS | ASESOR DE VENTAS', '', '222 228 54 78 | 222 155 69 19', '', 'alfredo.perez@ginsa.com.mx ', 'Estandar', 'Inactivo', 0),
(151, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MIGUEL ANGEL', 'PEREZ', 'RINCON', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS SEMINUEVOS', '', '222 228 54 78 | 222 447 05 13', '', 'miguelangel.perez@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(152, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ENRIQUE', 'SANCHEZ', 'BECERRA', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SEMINUEVOS | ASESOR DE VENTAS', '', '222 228 54 78 | 222 706 63 00', '', 'enrique.sanchez@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(153, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'RODRIGO', 'GARCIA', 'CIANCA', 'SERVICIO INTEGRAL GARCIA PINEDA S.A DE C.V', 'GERENTE GENERAL', 'SERVICIO', '222 246 46 66', '4530', 'rodrigo.garcia@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(154, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'TERESA', 'CRUZ', 'BARRIOS', 'SERVICIO INTEGRAL GARCIA PINEDA S.A DE C.V', 'ADMINISTRACION / ASISTENTE DE GTE. ADMINISTRATIVO', 'SERVICIO', '222 282 64 00', '4531', 'teresa.cruz@ginsa.com.mx', 'Estandar', 'Activo', 3514),
(155, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'DANIEL RODRIGO', 'LOAIZA', 'RODRIGUEZ', 'SERVICIO INTEGRAL GARCIA PINEDA S.A DE C.V', 'JEFE DE TALLER', 'SERVICIO', '222 282 64 00', '4538', 'daniel.loaiza@ginsa.com.mx', 'Estandar', 'Activo', 9451),
(156, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOSE LUIS', 'BARRALES', 'RIOS', 'SERVICIO INTEGRAL GARCIA PINEDA S.A DE C.V', 'ASESOR DE SERVICIO', 'SERVICIO', '222 282 64 00', '4534', 'asesorhyp@ginsa.com.mx', 'Estandar', 'Activo', 6540),
(157, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'OSCAR', 'GONZALEZ', 'TORIZ', 'SERVICIO INTEGRAL GARCIA PINEDA S.A DE C.V', 'ENCARGADO DE REFACCIONES', 'SERVICIO', '222 282 64 00', '4540', 'refaccionesserint@ginsa.com.mx', 'Estandar', 'Inactivo', 7621),
(158, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOSE CARLOS', 'NOMBRE', 'DEL CARMEN', 'SERVICIO INTEGRAL GARCIA PINEDA S.A DE C.V', 'VALUACION', 'SERVICIO', '222 282 64 00', '4536', 'valuadorhypgp2000@ginsa.com.mx', 'Estandar', 'Activo', 1788),
(159, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOSE', 'ALVARADO', 'MUNIVE', 'SERVICIO INTEGRAL GARCIA PINEDA S.A DE C.V', 'VALUADOR HYP', '', '222 282 64 00', '4581', 'valuadorhypgp2000@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(160, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MAURICIO', 'GUERRERO', 'GAMEZ', 'SERVICIO INTEGRAL GARCIA PINEDA S.A DE C.V', 'VALUACION', 'SERVICIO', '222 282 64 00', '4537', 'hypgp2000@ginsa.com.mx', 'Estandar', 'Activo', 6540),
(161, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ROXANA', 'HERNANDEZ', 'CORDERO', 'GINSA', 'CONTABILIDAD', '', '222 249 56 56', '.', 'roxana.cordero@ginsa.com.mx', 'Estandar', 'Activo', 0),
(162, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JAVIER', 'PEREZ', 'RAMIREZ', 'GINSA', 'CONTABILIDAD', '', '222 249 56 56', '.', 'javier.perez@ginsa.com.mx', 'Estandar', 'Activo', 0),
(164, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'LEONOR', 'HERNANDEZ', 'VILLARCE', 'SERVICIO INTEGRAL GARCIA PINEDA S.A DE C.V', 'VALUACION', 'SERVICIO', '222 282 64 00', '4537', 'hypgp2000@ginsa.com.mx', 'Estandar', 'Inactivo', 3240),
(165, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JORGE', 'SODI', 'ISA', 'GINSA', 'PRESIDENCIA', '', '222 249 56 56', '.', 'jorge.sodi@ginsa.com.mx', 'Estandar', 'Activo', 0),
(166, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARCO ANTONIO', 'CRUZ', 'QUIROZ', 'GINSA', 'JURIDICO', '', '222 249 56 56', '.', 'marco.cruz@ginsa.com.mx', 'Estandar', 'Activo', 0),
(167, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ITZAMA', 'GARCIA', '.', 'GINSA', 'JURIDICO', '', '222 249 56 56', '.', 'itzama.garcia@ginsa.com.mx', 'Estandar', 'Activo', 0),
(168, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOSE FRANCISCO', 'PEREZ', 'POLO', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ASESOR DE VTAS. SUAUTO', 'VENTAS', '222 166 22 56', '4072, 4073, 4074', 'francisco.perez@ginsa.com.mx', 'Estandar', 'Activo', 1310),
(169, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ABIGAIL', 'VEGA', 'SANTIAGO', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'CONTACT CENTER | ASESOR TEL.', 'BDC POSTVENTA', '222 240 00 21', '4022', 'contactcenter4@ginsa.com.mx', 'Estandar', 'Activo', 4271),
(171, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ELISEO', 'GUTIERREZ', 'MELCHOR', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'REFACCIONES | VTAS. COLISIÃ“N', 'REFACCIONES', '222 240 00 21', '4041', 'colisiongpa@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(172, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARCO ANTONIO', 'ALVA', 'OSORIO', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 524 07 94', '4075', 'marcoantonio.alva@ginsa.com.mx', 'Estandar', 'Activo', 6031),
(173, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ENRIQUE', 'BONET', 'LOPEZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 414 84 83', '4076', 'enrique.bonet@ginsa.com.mx', 'Estandar', 'Activo', 6032),
(174, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARCO ANTONIO', 'VAZQUEZ', 'SANCHEZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 240 0021', '4071', 'marcoantonio.vazquez@ginsa.com.mx', 'Estandar', 'Activo', 1254),
(176, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'CONTACT', 'CENTER', '.', 'GARCIA PINEDA ANGELOPOLIS', 'CONTACT CENTER', 'BDC POSTVENTA', '222 225 92 90', '4020', '.', 'Estandar', 'Activo', 0),
(177, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARIA FERNANDA', 'BAZAGOITIA', 'IRIGOYEN', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 292 10 11', '4077', 'fernanda.bazagoitia@ginsa.com.mx', 'Estandar', 'Activo', 6410),
(178, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MOSTRADOR', 'REFACCIONES', '.', 'GARCIA PINEDA ANGELOPOLIS', 'REFACCIONES / MOSTRADOR', 'REFACCIONES', '222 240 21 00', '4042', '.', 'Estandar', 'Activo', 0),
(179, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARIA FERNANDA', 'MENDEZ', 'DOMINGUEZ', 'PARISAUTO DE PUEBLA S.A. DE C.V.', 'SERVICIO | ASESOR DE CITAS', 'SERVICIO', '222 246 46 66', '4232', 'citas.servicio@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(180, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'VIGILANCIA/OPERADORA', '/', 'OPERADORA', 'GARCIA PINEDA 2000', 'VIGILANCIA', 'VENTAS', '222 282 64 00', '4550', '.', 'Estandar', 'Activo', 0),
(181, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MOISES IGNACIO', 'CHAVEZ', 'RODRIGUEZ', 'PARISAUTO DE SATELITE S.A. DE C.V.', 'JEFE DE HOJALATERIA Y PINTURA', 'SERVICIO', '555 393 17 28', '4635', 'hypsatelite@ginsa.com.mx', 'Estandar', 'Inactivo', 5041),
(182, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'HECTOR', 'ANDRADE', 'CRUZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 240 00 21 22.23.63.71.68', '4072, 4073, 4074', 'hector.andrade@ginsa.com.mx', 'Estandar', 'Activo', 7142),
(183, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'PAOLA', 'LOPEZ', 'BARRANCO', 'PARISAUTO DE PUEBLA S.A. DE C.V.', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 246 46 66', '4262', 'paola.lopez@ginsa.com.mx', 'Estandar', 'Activo', 0),
(185, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARIA DEL SOCORRO', 'ORATO', 'RAMÃREZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'FLOTILLAS', 'VENTAS', '222 650 35 85', '4560', 'gmfleetservices@ginsa.com.mx ', 'Estandar', 'Inactivo', 0),
(187, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ORELLY', 'HERNÃNDEZ', 'RENDÃ“N', 'GarcÃ­a Pineda AngelÃ³polis S.A. de C.V.', 'SEMINUEVOS', '', '222 228 54 78 |  2227096605', '', 'orelly.hernandez@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(188, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARIA MARGARITA', 'CERVANTES', 'ROBLEDO', 'PARISAUTO DE SATELITE S.A. DE C.V.', 'ADMINISTRACIÃ“N | CAJERA', 'ADMINISTRACION', '555 393 17 28', '4606', 'caja.satelite@ginsa.com.mx', 'Estandar', 'Activo', 9173),
(189, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JESUS', 'FUEYO', 'MARTINEZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SEMINUEVOS | GERENTE SEMINUEVOS', '', '222 282 54 78 | 222 228 62 90', '', 'jesus.fueyo@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(190, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'URSULA DOLORES', 'HERNANDEZ', 'GUERRERO', 'PARISAUTO DE PUEBLA S.A. DE C.V.', 'MERCADOTECNIA | RESPONSABLE VN MARKETING', 'CONTACT CENTER', '222 246 46 66', '4222', 'contactovn.parisauto@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(191, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'LEODINA', 'GONZALEZ', 'VEGA', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ADMON. VENTAS', 'BDC POSTVENTA', '222 240 00 21', '4025', 'leodina.gonzalez@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(192, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'VERONICA', 'RAMOS', 'SANCHEZ', 'PARISAUTO DE SATELITE S.A. DE C.V.', 'VENTAS | ENCARGADO DE VEHICULOS NUEVOS', 'VENTAS', '555 393 17 28', '4654', 'entregasvn.parisauto.sat@ginsa.com.mx', 'Estandar', 'Activo', 1719),
(193, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MAGALI', 'PONCE', 'CERVANTES', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'CONTACT CENTER | ASESOR TEL.', 'BDC POSTVENTA', '222 240 00 21', '4023', 'contactcenter3@ginsa.com.mx', 'Estandar', 'Activo', 5120),
(194, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'GERMAN', 'MOTA', 'PONCE', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SERVICO | CONTROL DE CALIDAD', 'SERVICIO', '222 240 00 21', '4038', 'german.mota@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(195, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'IVONNE', 'SOLAR', 'DIAZ', 'GARCIA PINEDA ANGELOPOLIS SA DE CV', 'RECURSOS HUMANOS', 'ADMINISTRACION', '222 240 00 21', '4001', 'ivonne.solar@ginsa.com.mx', 'Estandar', 'Activo', 7308),
(196, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ALEJANDRA', 'CRUZ', 'ROMERO', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | JEFE DE PISO', 'VENTAS', '222 240 00 21', '4050', 'recepciongpa@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(197, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'KAREN ALICIA', 'TALAVERA', 'BERMUDEZ', 'PARISAUTO DE PUEBLA S.A. DE C.V.', 'HOSSTES TARDE', 'VENTAS', '222 246 46 66', '4250', 'recepcion.puebla@ginsa.com.mx', 'Estandar', 'Activo', 0),
(198, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARIA FERNANDA', 'MUÃ‘OZ ', 'BARRANCO', 'PARISAUTO DE PUEBLA S.A. DE C.V.', 'HOSSTES MAÃ‘ANA', 'VENTAS', '222 246 46 66', '4250', 'recepcion.puebla@ginsa.com.mx', 'Estandar', 'Activo', 6012),
(199, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JUAN', 'HERNANDEZ', 'PACHECO', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'FLOTILLAS', 'VENTAS', '222 650 35 85', '4561', 'juan.hernandez@ginsa.com.mx', 'Estandar', 'Activo', 0),
(200, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'CLAUDIA VIRGINIA', 'ELVIRA', 'LIMON', 'PARISAUTO DE PUEBLA S.A. DE C.V.', 'SERVICIO | ASESOR DE CITAS', 'CITAS', '222 246 46 66', '4232', 'citas.servicio@ginsa.con.mx', 'Estandar', 'Activo', 6013),
(201, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'OSCAR', 'MORALES', 'OREA', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'REFACCIONES | VTAS. COLISIÃ“N', 'REFACCIONES', '222 240 00 21', '4041', 'colisiongpa@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(202, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARIO', 'RUIZ DEL SOL', 'URIBE', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'GERENTE DE SERVICIO', 'SERVICIO', '222 240 00 21', '4030', 'mario.ruizdelsol@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(203, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ELISEO', 'GUTIERREZ', 'MELCHOR', 'PARISAUTO DE PUEBLA S.A. DE C.V.', 'REFACCIONES | VTAS. COLISIÃ“N', 'REFACCIONES', '222 246 46 66', '4241', 'colision.pap@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(204, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ROBERTO', 'ROJAS', 'JUAREZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SEMINUEVOS | ASESOR DE VENTAS', '', '222 228 54 78', '', 'roberto.rojas@ginsa.com.mx', 'Estandar', 'Inactivo', 8044),
(205, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARIA TERESA', 'ESQUIVEL', 'CUE', 'PARISAUTO DE SATELITE S.A. DE C.V.', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '555 393 17 28', '4661, 4662, 4663, 4665', 'teresa.esquivel@ginsa.com.mx', 'Estandar', 'Activo', 3472),
(206, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'TANIA ESTELA', 'VARGAS', 'CANDELARIO', 'PARISAUTO DE PUEBLA S.A. DE C.V.', 'MERCADOTECNIA | RESPONSABLE  PV MARKETING', 'CONTACT CENTER', '222 246 46 66', '4221', 'contactopv.parisauto@ginsa.com.mx', 'Estandar', 'Activo', 9012),
(207, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'VICTOR JESÃšS', 'SANCHEZ', 'ZUÃ‘IGA', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'REFACCIONES | ALMACEN Y VENTAS TALLER', 'REFACCIONES', '222 240 00 21', '4044', 'ventanillagpa@ginsa.com.mx', 'Estandar', 'Activo', 8014),
(208, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ULISES', 'PIZ', 'HERNANDEZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SERVICIO | CONTROL DE CALIDAD', 'SERVICIO', '222 240 00 21', '4038', 'calidad.gpa@ginsa.com.mx', 'Estandar', 'Activo', 8021),
(209, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'RAFAEL', 'RIVERA', 'PEREZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SEMINUEVOS | ASESOR DE VENTAS', '', '222 282 54 78 | 222 228 62 90', '', 'rafael.rivera@ginsa.com.mx', 'Estandar', 'Activo', 0),
(210, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JORGE', 'NAVARRETE', 'ANTONIO', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SEMINUEVOS | ASESOR DE VENTAS', '', '222 282 54 78 | 222 228 62 90', '', 'jorge.navarrete@ginsa.com.mx', 'Estandar', 'Activo', 0),
(211, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'EDGAR LEOBARDO', 'MUJICA', 'HERNANDEZ', 'PARISAUTO DE PUEBLA S.A. DE C.V.', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 246 46 66', '4264', 'edgar.mujica@ginsa.com.mx', 'Estandar', 'Inactivo', 9023),
(212, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOSE VICTOR', 'TELLEZ', 'ECHAVARRIA', 'PARISAUTO DE PUEBLA S.A. DE C.V.', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 246 46 66', '4264', 'victor.tellez@ginsa.com.mx', 'Estandar', 'Activo', 0),
(213, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ALEJANDRA', 'ZEPEDA', 'VAZQUEZ', 'PARISAUTO DE PUEBLA S.A. DE C.V.', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 246 46 66', '4261', 'alejandra.zepeda@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(214, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOVAN', 'BAUTISTA', 'HUERTA', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'GERENTE DE SERVICIO', 'SERVICIO', '222 240 00 21', '4030', 'jovan.bautista@ginsa.com.mx', 'Estandar', 'Activo', 8024),
(215, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'BERENICE', 'ARAUJO', 'NIETO', 'PARISAUTO DE SATELITE S.A. DE C.V.', 'VENTAS | HOSTESS', 'VENTAS', '555 393 17 28', '4650', 'recepcion.satelite@ginsa.com.mx', 'Estandar', 'Activo', 5021),
(216, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'FERNANDO', 'SANCHEZ', 'FERNANDEZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SEMINUEVOS | ASESOR DE VENTAS', '', '222 228 54 78 | 222 228 62 90', '', 'fernando.sanchez@ginsa.com.mx', 'Estandar', 'Activo', 0),
(217, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'RUBEN', 'GUTIERREZ', 'ALVAREZ', 'PARISAUTO DE SATELITE S.A. DE C.V.', 'SERVICIO | ASESOR DE SERVICIO', 'SERVICIO', '555 393 17 28', '4634', 'ruben.gutierrez@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(218, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JESUS', 'FUEYO', 'MARTINEZ', '	GARCIA PINEDA ANGELOPOLIS S.A DE C.V', '', 'SERVICIO', '222 282 54 78 | 222 228 62 90', '', 'jesus.fueyo@ginsa.com.mx', 'Estandar', 'Activo', 0),
(219, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ROBERTO', 'ROJAS', 'JUAREZ', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SEMINUEVOS | VALUADOR DE AUTOS', 'VENTAS', '222 240 00 21', '4060', 'roberto.rojas@ginsa.com.mx', 'Estandar', 'Activo', 0),
(220, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'KARINA', 'TORRES', 'BELLO', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | JEFE DE PISO', 'VENTAS', '222 240 00 21', '4050', 'recepciongpa@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(221, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ALFONSO CESAR', 'RAMIREZ', 'PALMA', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | GERENTE DE VENTAS', 'VENTAS', '222 240 00 21', '4051', 'alfonso.ramirez@ginsa.com.mx', 'Estandar', 'Inactivo', 8051),
(222, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'OMAR', 'CALI', 'HERNANDEZ', 'SERVICIO INTEGRAL GARCIA PINEDA S.A DE C.V', 'ENCARGADO DE REFACCIONES', 'REFACCIONES', '222 282 64 00', '4540', 'refaccionesserint@ginsa.com.mx', 'Estandar', 'Activo', 0),
(223, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ALEJANDRO', 'ORTEGA', 'ROEDEL', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 240 00 21     2228246069', '4068', 'alejandro.ortega@ginsa.com.mx', 'Estandar', 'Activo', 0),
(225, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOSE ALFREDO', 'CLARA', 'GARCÃA', 'PARISAUTO DE SATELITE S.A. DE C.V.', 'REFACCIONES | ALMACEN Y VENTAS TALLER', 'REFACCIONES', '555 393 17 28', '4641', 'refac.parisauto.satelite@ginsa.com.mx', 'Estandar', 'Activo', 5051),
(226, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOSE', 'MENDEZ', 'TORRES', 'PARISAUTO DE PUEBLA S.A. DE C.V.', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 246 46 66', '4261', 'jose.mendez@ginsa.com.mx', 'Estandar', 'Activo', 9052),
(227, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MIRIAM', 'RODRIGUEZ', 'ROSAS', 'PARISAUTO DE SATELITE S.A. DE C.V.', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '555 393 17 28', '4661, 4662, 4663, 4665', 'miriam.ramirez@ginsa.com.mx', 'Estandar', 'Activo', 0);
INSERT INTO `usuarios` (`id`, `usuario`, `contrasena`, `Nombre`, `Paterno`, `Materno`, `Empresa`, `Departamento`, `deptoTari`, `NumTlefonico`, `Ext`, `Email`, `tipoUsuario`, `estatus`, `AuthCode`) VALUES
(228, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MONICA LILIANA', 'CORTEZ', 'RODRIGUEZ', 'PARISAUTO DE PUEBLA S.A. DE C.V.', 'MERCADOTECNIA | RESPONSABLE VN MARKETING', 'CONTACT CENTER', '222 246 46 66', '4222', 'contactopv.parisauto@ginsa.com.mx', 'Estandar', 'Activo', 9061),
(229, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ALEJANDRO', 'VELEZ', 'ORTEGA', 'PARISAUTO DE PUEBLA S.A. DE C.V.', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 246 46 66', '4261', 'alejandro.velez@ginsa.com.mx', 'Estandar', 'Activo', 9062),
(230, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOSE RAMON', 'RODRIGUEZ', 'GUERRERO', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 240 00 21', '4079', 'joseramon.rodriguez@ginsa.com.mx', 'Estandar', 'Activo', 0),
(231, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOSE EDUARDO', 'CASTILLO', 'SPINDOLA', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ASESOR DE VTAS. LEADIT', 'VENTAS', '222 569 04 54', '4066', 'eduardo.castillo@ginsa.com.mx', 'Estandar', 'Activo', 8061),
(232, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARIA ANDREA', 'DOMINGUEZ', 'AGUILAR', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | JEFE DE PISO', 'VENTAS', '222 240 00 21', '4050', 'recepciongpa@ginsa.com.mx', 'Estandar', 'Activo', 0),
(233, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'TAHITI', 'GUEVARA', 'BRIONES', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | HOSTESS', 'VENTAS', '222 240 00 21', '4050', 'hostessgpa@ginsa.com.mx', 'Estandar', 'Activo', 8062),
(234, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARCOS', 'CARDENAS', 'NAVARRO', 'PARISAUTO DE SATELITE S.A. DE C.V.', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '555 393 17 28', '4661, 4662, 4663, 4665', 'marcos.cardenas@ginsa.com.mx', 'Estandar', 'Inactivo', 5061),
(235, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOSE LUIS', 'VALENCIA', 'OSIO', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 254 20 75', '4080', 'joseluis.valencia@ginsa.com.mx', 'Estandar', 'Activo', 0),
(236, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ANABEL', 'ANGULO', 'RAMIREZ', 'SERVICIO INTEGRAL GARCIA PINEDA S.A DE C.V', 'VALUACION', 'SERVICIO', '222 282 64 00', '4535', 'asesorhyp2@ginsa.com.mx', 'Estandar', 'Activo', 0),
(237, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'DIANA', 'MARTINEZ', 'SALGADO', 'GARCIA PINEDA ANGELOPOLIS S.A. DE C.V.', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '551 295 89 86', '4081', 'diana.martinez@ginsa.com.mx', 'Estandar', 'Activo', 8066),
(238, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOSE ALBERTO', 'MORALES', 'LIMA', 'PARISAUTO DE PUEBLA S.A. DE C.V.', 'REFACCIONES | VTAS. COLISIÃ“N', 'REFACCIONES', '222 246 46 66', '4241', 'colision.pap@ginsa.com.mx', 'Estandar', 'Activo', 9713),
(239, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'DORIAN', 'AGUILAR', 'SOSA', 'PARISAUTO DE PUEBLA S.A. DE C.V.', 'REFACCIONES | MOSTRADOR', 'REFACCIONES', '222 246 46 66', '4242', 'mostrador.pap@ginsa.com.mx', 'Estandar', 'Activo', 0),
(240, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JESUS ERNESTO', 'CABALLERO', 'CUREÃ‘O', 'PARISAUTO DE SATELITE S.A. DE C.V.', 'SERVICIO | ASESOR DE SERVICIO', 'SERVICIO', '555 393 17 28', '4634', 'jesus.caballero@ginsa.com.mx', 'Estandar', 'Inactivo', 5062),
(241, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'LUIS EDUARDO', 'REYES', 'SANCHEZ', 'GARCIA PINEDA ANGELOPOLIS S.A. DE C.V.', 'VENTAS | ASESOR DE VENTAS', '', '222 240 00 21', '', 'luis.reyes@ginsa.com.mx', 'Estandar', 'Activo', 8069),
(242, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MARIA BEATRIZ', 'SANCHEZ', 'TOBÃ“N', 'GARCIA PINEDA ANGELOPOLIS S.A. DE C.V.', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 706 24 82', '4061', 'beatriz.sanchez@ginsa.com.mx', 'Estandar', 'Activo', 8065),
(243, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JOSUE FRANCISCO', 'CASTILLO', 'AZUARA', 'GARCIA PINEDA ANGELOPOLIS S.A. DE C.V.', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '782 123 43 02', '4082', 'josue.castillo@ginsa.com.mx', 'Estandar', 'Activo', 0),
(244, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'LUIS MIGUEL', 'RAMIREZ', 'DE JESUS', 'GARCIA PINEDA ANGELOPOLIS S.A. DE C.V.', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 188 3085', '4083', 'miguel.ramirez@ginsa.com.mx', 'Estandar', 'Activo', 0),
(245, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'LEVI', 'GARCIA', 'MANCHIA', 'GARCIA PINEDA ANGELOPOLIS S.A. DE C.V.', 'SEMINUEVOS | ASESOR DE VENTAS', '', '222 228 54 78', '', 'levi.garcia@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(246, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'NORA LILIA', 'HUERTA', 'ROLDAN', 'GARCIA PINEDA ANGELOPOLIS S.A. DE C.V.', 'VENTAS | ADMON. VENTAS', 'BDC POSTVENTA', '222 240 00 21', '4025', 'nora.huerta@ginsa.com.mx', 'Estandar', 'Activo', 8161),
(247, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'FLOR CRISTELA', 'CONDE', 'REYES', 'GARCIA PINEDA ANGELOPOLIS S.A. DE C.V.', 'ADMINISTRACIÃ“N | CAJERA', 'ADMINISTRACION', '222 240 00 21', '4006', 'caja.angelopolis@ginsa.com.mx', 'Estandar', 'Activo', 0),
(248, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JAVIER', 'SANDOVAL', 'MORENO', 'PARISAUTO DE PUEBLA S.A. DE C.V.', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 246 46 66', '', 'javier.sandoval@ginsa.com.mx', 'Estandar', 'Activo', 0),
(250, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'PAULA', 'CHAVELAS', 'BENITEZ', 'PARISAUTO DE SATELITE S.A. DE C.V.', 'SEGUROS | ASESOR DE SEGUROS', 'ADMINISTRACION', '555 393 17 28', '4607', 'paula.chavelas@ginsa.com.mx', 'Estandar', 'Inactivo', 5071),
(251, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'PAULA', 'CHAVELAS', 'BENITEZ', 'PARISAUTO DE SATELITE S.A. DE C.V.', 'SEGUROS | ASESOR DE SEGUROS', 'ADMINISTRACION', '555 393 17 28', '4607', 'paula.chavelas@ginsa.com.mx', 'Estandar', 'Activo', 5071),
(252, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ABRAHAM', 'CORTES', 'ARENAS', 'GARCIA PINEDA ANGELOPOLIS S.A. DE C.V.', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222  255 70 55', '4085', 'abraham.cortes@ginsa.com.mx', 'Estandar', 'Activo', 8072),
(253, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ANTONIO', 'TEJEDA', 'PARRA', 'GARCIA PINEDA ANGELOPOLIS S.A. DE C.V.', 'SEMINUEVOS | ASESOR DE VENTAS', '', '222 228 54 78 | 222 228 62 90', '', 'antonio.tejeda@ginsa.com.mx', 'Estandar', 'Inactivo', 0),
(255, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MIGUEL ANGEL', 'ESPINOZA', 'AMBRIZ', 'PARISAUTO DE SATELITE S.A. DE C.V.', 'JEFE DE HOJALATERIA Y PINTURA', 'SERVICIO', '555 393 17 28', '4635', 'hypsatelite@ginsa.com.mx', 'Estandar', 'Activo', 5072),
(256, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MERCEDES ARLET', 'PONCE ', 'MARTINEZ', 'GARCIA PINEDA ANGELOPOLIS S.A. DE C.V.', 'VENTAS | ASESOR DE VENTAS', 'VENTAS', '222 240 00 21', '4072, 4073, 4074', 'arlet.ponce@ginsa.com.mx', 'Estandar', 'Inactivo', 8053),
(257, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'DANIELA', 'TALAVERA', 'CORNEJO', 'GARCÃA PINEDA ANGELOPOLIS S.A. DE C.V.', 'VENTAS | AUXILIAR DE RESPONSE', 'VENTAS', '222 240 00 21', '', 'daniela.talavera@ginsa.com.mx', 'Estandar', 'Inactivo', 8064),
(258, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'MOISES IGNACIO', 'CHAVEZ', 'RODRIGUEZ', 'PARISAUTO DE SATELITE S.A. DE C.V.', 'SERVICIO | ASESOR DE SERVICIO', 'SERVICIO', '555 393 17 28', '4634', 'moises.chavez@ginsa.com.mx', 'Estandar', 'Activo', 5081),
(259, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ANGELA ANDREA', 'RODRIGUEZ ', 'CONDE', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'SERVICIO', 'SERVICIO', '222 240 00 21', '4032', 'andrea.rodriguez@ginsa.com.mx', 'Estandar', 'Activo', 8043),
(260, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'JESUS', 'QUICEHUATL', 'AVILA', 'PARISAUTO DE PUEBLA S.A. DE C.V.', 'SERVICIO | RESPONSABLE DE CALIDAD', 'SERVICIO', '222 246 46 66', '', 'calidad.ppu@ginsa.com.mx', 'Estandar', 'Activo', 9081),
(261, '24!2ji%#fg,a1411', '78323%#1a54!Df,$', 'ILIANA ', 'CALDERON', 'SANCHEZ', 'PARISAUTO DE PUEBLA S.A. DE C.V.', 'SERVICIO | ASIST. GERENCIA SERV.', 'SERVICIO', '222 246 46 66', '4231', 'asistger.serpue@ginsa.com.mx', 'Estandar', 'Activo', 1207),
(262, 'prueba', 'prueba', 'Prueba', 'Prueba', 'Prueba', 'GARCIA PINEDA ANGELOPOLIS S.A DE C.V', 'REFACCIONES', 'REFACCIONES', '231231213213', '9632', 'prueba@hotmail.com', 'Estandar', 'Activo', 8989);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=263;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
