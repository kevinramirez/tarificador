# Tarificador

Sitio desarrollado en PHP 7 y MySQL con Bootstrap y gráficas charset

### Descripción

Su función principal es obtener los datos de un conmutador la cual se registraron las llamadas realizadas, lo que el sistema se va a encargar es dar un reporte mensual de la cantidad de llamadas y la duración de las mismas visualizando con gráficos y registros en tablas. Los reportes están divididos por departamentos y como ya se había mencionado anteriormente es mensual.

### Instalación

Para que pueda funcionar correctamente, aquí se encuentran dos archivos SQL las cuales contienen los datos de los usuarios y las llamadas realizadas.

1. crear una base de datos llamada 'smrd'

2. importar los dos archivos 'angelopolis(servidor).slq' y 'usuarios(servidor).sql' a la base de datos anteriormente creada.

Para obtener acceso al sistema colocar los siguientes datos:

Usuario: admin
Contraseña: Ginsa200

Los datos solo corresponde a solo un mes ya que es un ejemplo, el mes correspondiente es de Junio de 2006 así que para obtener un reporte de las llamadas realizadas de ese mes colocar 01/06/2016 hasta 01/07/2016.